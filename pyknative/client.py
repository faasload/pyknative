from typing import Optional

from kubernetes import config, client
from pyknative.constant import KNATIVE_EXTERNAL_KOURIER_SERVICE, KNATIVE_KOURIER_NAMESPACE

from pyknative.models import ApiConfiguration, Configuration


def get_api_configuration(configuration: Optional[Configuration] = None) -> ApiConfiguration:
    if configuration:
        config.load_kube_config_from_dict(
            config_dict={
                "clusters": [
                    {
                        "name": "default",
                        "cluster": {
                            "server": configuration["server"]
                        }
                    }
                ],
                "contexts": [
                    {
                        "name": "default", 
                        "context": {
                            "cluster": "default",
                            "namespace": "default",
                            "user": "default"
                        }
                    }
                ],
                "users": [
                    {
                        "name": "default",
                        "user": {
                            "token": configuration["key"],
                            "username": "default", # not used
                            "password": "default" # not used
                        }
                    }
                ]
            },
            context="default"
        )
    else:
        config.load_kube_config()

    core_v1 = client.CoreV1Api()
    kourier_service: client.V1Service = core_v1.read_namespaced_service(name=KNATIVE_EXTERNAL_KOURIER_SERVICE, namespace=KNATIVE_KOURIER_NAMESPACE)
    kourier_pods = core_v1.list_namespaced_pod(namespace=KNATIVE_KOURIER_NAMESPACE)
    if not kourier_service.spec:
        raise ValueError("Cannot load the configuration for the gateway")
    selector = kourier_service.spec.selector['app']
    kourier_pod = next(filter(lambda x: x.metadata.labels.get('app', '') == selector, kourier_pods.items))
    configu = ApiConfiguration(
        ip=kourier_service.spec.cluster_ip if kourier_service.spec is not None else "",
        tracer_ip='192.168.49.2',
        gateway_component_name=kourier_pod.metadata.name if kourier_pod.metadata else ""
    )
    print(configu)
    return configu
    
