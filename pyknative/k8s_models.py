from typing import Any, List, TypedDict


class K8sContainerEnv(TypedDict):
    name: str
    value: Any


class K8sContainer(TypedDict):
    image: str
    env: List[K8sContainerEnv]


class K8sMetadata(TypedDict):
    name: str


class K8sTemplateSpec(TypedDict):
    containers: List[K8sContainer]


class K8sTemplate(TypedDict):
    spec: K8sTemplateSpec


class K8sSpec(TypedDict):
    template: K8sTemplate


class K8sObject(TypedDict):
    apiVersion: str
    kind: str
    metadata: K8sMetadata
    spec: K8sSpec
