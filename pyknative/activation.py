import json
from typing import List, Tuple
from kubernetes.client import CoreV1Api
from kubernetes.client.models import V1Pod
from pyknative.models import ApiConfiguration, Activation

from pyknative.constant import KNATIVE_KOURIER_NAMESPACE, KNATIVE_QUEUE_PROXY, KNATIVE_USER_CONTAINER


def _get_trace_details(trace_id: str, cfg: ApiConfiguration) -> Tuple[int, int, int]:
    import requests as r
    result = r.get(
        url=f"http://{cfg.tracer_ip}:16686/api/traces/{trace_id}"
    )

    processes = result.json()['data'][0]["processes"]
    activator_process = ""
    for k, v in processes.items():
        if v["serviceName"] == "activator-service":
            activator_process = k

    # If no process id for the service then no data availables, hence 0 will be send
    if not activator_process:
        return 0, 0, 0

    spans = result.json()['data'][0]["spans"]
    activator_throttler = next(filter(lambda x: x['processID'] == activator_process and x['operationName'] == "throttler_try", spans))
    activator_global = next(filter(lambda x: x['processID'] == activator_process and x['operationName'] == "/" and not x["references"], spans))

    start_time_µs = activator_throttler.get("startTime", 0)
    end_time_µs = activator_global.get("startTime", 0) + activator_global.get("duration", 0)
    init_time_µs = activator_throttler.get("duration", 0)

    return (
        int(str(start_time_µs)[:-3]), 
        int(str(end_time_μs)[:-3]), 
        0 if init_time_µs < 1000 else int(str(init_time_µs)[:-3]), 
    )


def _get_wait_time_from_ingress_logs(request_id: str, ingress_pod_name: str, api_instance: CoreV1Api) -> int:
    import re
    result = api_instance.read_namespaced_pod_log(
        name=ingress_pod_name, 
        namespace=KNATIVE_KOURIER_NAMESPACE, 
        _preload_content=False,
    )
    request_log = next(filter(lambda x: request_id in str(x), result), None) 
    s = re.search(pattern=r'(?P<duration>\d+) \d+ "', string=str(request_log))
    if request_log is None or s is None:
        return 0

    return int(s.group("duration"))


def _get_queue_proxy_logs(p: V1Pod, namespace: str, action_names: List[str], api_instance: CoreV1Api, cfg: ApiConfiguration, number_of_lines: int = 500) -> List[Activation]:
    name = p.metadata.name if p.metadata else ""
    logs: List[Activation] = []
    if next(filter(lambda x: name.startswith(x), action_names), None) and p.status and p.status.phase == 'Running':
        container_id = next(
            filter(
                lambda x: x.name == KNATIVE_USER_CONTAINER, 
                p.status.container_statuses
            ), 
            type('',(object,),{"container_id": ""})
        ).container_id
        result = api_instance.read_namespaced_pod_log(
            name=name, 
            namespace=namespace, 
            container=KNATIVE_QUEUE_PROXY,
            _preload_content=False,
            tail_lines=number_of_lines
        )
        for log in result:
            dump = json.loads(log)
            if 'activationId' not in dump:
                continue
            request_id = dump['requestId'][1:-1]
            trace_id = dump['traceId'][1:-1]
            start_time, end_time, init_time = _get_trace_details(trace_id=trace_id, cfg=cfg)
            time_from_gateway_ms = _get_wait_time_from_ingress_logs(request_id=request_id, ingress_pod_name=cfg.gateway_component_name, api_instance=api_instance)
            wait_time = 0 if time_from_gateway_ms == 0 else time_from_gateway_ms - (end_time - start_time)
            logs.append(
                Activation(
                    activation_id=dump['activationId'][1:-1],
                    trace_id=dump['traceId'][1:-1],
                    docker_id=container_id,
                    name=name.split("-")[0],
                    start=start_time,
                    end=end_time,
                    init=init_time,
                    wait=wait_time
                )
            )
    return logs


def get_executed_activations(cfg: ApiConfiguration) -> List[Activation]:
    import pyknative.action as action
    action_names = action.get_names_by_namespace()
    api_instance = CoreV1Api()
    logs = []
    for k, v in action_names.items():
        pods = api_instance.list_namespaced_pod(k)
        for p in pods.items:
            logs = logs + _get_queue_proxy_logs(p=p, namespace=k, action_names=v, api_instance=api_instance, cfg=cfg)
    return logs

