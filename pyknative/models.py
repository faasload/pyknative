from typing import List, NamedTuple, TypedDict, Union
from dataclasses import dataclass, field

from pyknative.constant import KNATIVE_SERVICES_API_VERSION
from pyknative.k8s_models import K8sContainerEnv, K8sObject


class ApiConfiguration(NamedTuple):
    ip: str # Internal IP of the gateway
    tracer_ip: str # get all the tracing
    gateway_component_name: str # Use to get the logs of the pod of the gateway


class Configuration(TypedDict):
    server: str
    key: str


@dataclass
class Action:
    name: str
    image: str
    env: List[K8sContainerEnv] = field(default_factory=list)

    def get_as_k8s_obj(self) -> K8sObject:
        return {
            "apiVersion": KNATIVE_SERVICES_API_VERSION,
            "kind": "Service",
            "metadata": {
                "name": self.name
            },
            "spec": {
                "template": {
                    "spec" : {
                        "containers": [
                            {
                                "env": self.env,
                                "image": self.image
                            }
                        ]
                    }
                }
            }
        }


@dataclass
class ActivationResult:
    status: str
    result: dict

class Activation(NamedTuple):
    activation_id: str
    trace_id: str
    docker_id: str
    name: Union[str, None] = None
    version: Union[str, None] = None
    start: Union[int, None] = None
    end: Union[int, None] = None
    init: Union[int, None] = None
    wait: Union[int, None] = None
