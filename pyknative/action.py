from typing import Any, Dict, List, Union
from kubernetes.client import CustomObjectsApi
from pyknative.constant import KNATIVE_SERVICE_GROUP, KNATIVE_SERVICE_PLURAL, KNATIVE_SERVICE_VERSION
from pyknative.exception import KnativeException

from pyknative.models import Action, ApiConfiguration


def get(action_name: str, namespace: str = "default") -> Action:
    return get_all_by_namespace(action_name, namespace)[namespace][0]


def create(action: Action, namespace: str = "default") -> None:
    _api_instance = CustomObjectsApi()
    try:
        _api_instance.create_namespaced_custom_object(
            group=KNATIVE_SERVICE_GROUP,
            version=KNATIVE_SERVICE_VERSION,
            namespace=namespace,
            plural=KNATIVE_SERVICE_PLURAL,
            body=action.get_as_k8s_obj()
        )
    except:
        raise KnativeException(f"Creation error: action '{action.name}' in namespace '{namespace}' already exists")


def delete(action_name: str, namespace: str = "default"):
    _api_instance = CustomObjectsApi()
    _api_instance.delete_namespaced_custom_object(
        group=KNATIVE_SERVICE_GROUP,
        version=KNATIVE_SERVICE_VERSION,
        name=action_name,
        namespace=namespace,
        plural=KNATIVE_SERVICE_PLURAL
    )


def invoke(action_name: str, config: ApiConfiguration, namespace: str = "default", params: Dict[str, Any] = {}) -> str:
    import requests as r
    from uuid import uuid4

    activation_id = str(uuid4())
    response = r.post(
        f"http://{config.ip}", 
        params=params, 
        headers={
            "activationId": activation_id, 
            "Host": f"{action_name}.{namespace}.{config.ip}.sslip.io",
        }
    )
    try:
       response.raise_for_status()
    except Exception as e:
        raise KnativeException(f"Invokation error for the action '{action_name}' in namespace '{namespace}' : {e}")
    finally:
        return activation_id


def get_all_by_namespace(action_name: Union[str, None] = None, namespace: Union[str, None] = None) -> Dict[str, List[Action]]:
    def _knative_service_to_action(resource: dict) -> Action:
        user_container = r['spec']["template"]["spec"]["containers"][0]
        return Action(
            name=resource['metadata']['name'],
            image=user_container['image'],
            env=user_container['env']
        )

    actions: Dict[str, List[Action]] = {}

    _api_instance = CustomObjectsApi()
    if action_name and namespace:
        resource = _api_instance.get_namespaced_custom_object(
            group=KNATIVE_SERVICE_GROUP,
            version=KNATIVE_SERVICE_VERSION,
            name=action_name,
            namespace=namespace,
            plural=KNATIVE_SERVICE_PLURAL
        )
        actions[namespace] = [_knative_service_to_action(resource)]
    else:
        resources = _api_instance.list_cluster_custom_object(
            group=KNATIVE_SERVICE_GROUP,
            version=KNATIVE_SERVICE_VERSION,
            plural=KNATIVE_SERVICE_PLURAL
        )
        for r in resources['items']:
            n = r['metadata']['namespace']
            if namespace in actions:
                actions[n].append(_knative_service_to_action(r))
            else:
                actions[n] = [_knative_service_to_action(r)]
    return actions


def get_names_by_namespace(action_name: Union[str, None] = None, namespace: Union[str, None] = None) -> Dict[str, List[str]]:
    actions: Dict[str, List[str]] = {}

    _api_instance = CustomObjectsApi()
    if action_name and namespace:
        resource = _api_instance.get_namespaced_custom_object(
            group=KNATIVE_SERVICE_GROUP,
            version=KNATIVE_SERVICE_VERSION,
            name=action_name,
            namespace=namespace,
            plural=KNATIVE_SERVICE_PLURAL
        )
        actions[namespace] = [resource['metadata']['name']]
    else:
        resources = _api_instance.list_cluster_custom_object(
            group=KNATIVE_SERVICE_GROUP,
            version=KNATIVE_SERVICE_VERSION,
            plural=KNATIVE_SERVICE_PLURAL
        )
        for r in resources['items']:
            n = r['metadata']['namespace']
            if n in actions:
                actions[n].append(r['metadata']['name'])
            else:
                actions[n] = [r['metadata']['name']]
    return actions
