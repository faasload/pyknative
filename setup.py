from setuptools import find_packages, setup

# Package meta-data.
NAME = 'pyknative'
DESCRIPTION = 'Python interface to Knative.'
URL = ''
EMAIL = 'viktor.colas@proton.me'
AUTHOR = 'Viktor Colas'
REQUIRES_PYTHON = '>=3.8.0'
VERSION = '0.1.0'

setup(
    name=NAME,
    version=VERSION,    
    description=DESCRIPTION,
    url=URL,
    author=AUTHOR,
    author_email=EMAIL,
    license='',
    packages=find_packages(exclude=["tests", "*.tests", "*.tests.*", "tests.*"]),
    install_requires=[
        'kubernetes'
    ],
    classifiers=[],
)

